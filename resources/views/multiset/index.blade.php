@extends('layouts.app')

@section('header')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

@endsection

@section('content')

    <div class="col-md-4">
        <h4>Multisets</a></h4>
    </div>
    <div class="form-group col-md-8">
        <a href="/multisets/importMulti" class="btn btn-danger">Upload file</a>
    </div>
    <hr>

    <div class="container col-md-8">
            <table class="table hover" id="multisets-table">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Multiset</th>
                    <th>Description</th>
                    <th>Qty</th>
                </tr>
                </thead>
                @foreach($multisets as $multiset)
                    <tr>
                        <td>
                            {{$multiset->id}}
                        </td>
                        <td>
                            <a href="">  {{$multiset->code}}  </a>
                        </td>
                        <td>
                            {{$multiset->description}}
                        </td>
                        <td>
                            {{$multiset->qty}}
                        </td>
                    </tr>
                @endforeach
            </table>

    </div>

    <div class="col-md-4">

        Multiset details go here by Vue.js on table item hover
    </div>

@endsection


@push('scripts')

    <script src="/js/dropzone.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

    <script>
        $(function () {
            $('#multisets-table').DataTable(
                {
                    "order": [[1, "asc"]],
                    "pageLength": 100
                }
            );
        });
    </script>

@endpush

