@extends('layouts.app')

@section('header')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.css">

@endsection

@section('content')

    <h4>Upload Multiset files</h4>

    <hr>

    <div class="col-md-12">
        <form action="/uploadMulti" method="POST" enctype="multipart/form-data" class="form-control">
            <input type="file" name="file"/>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Import file</button>
            </div>


            {{csrf_field()}}
        </form>


    </div>

@endsection


@push('scripts')

    <script src="/js/dropzone.js"></script>


@endpush

