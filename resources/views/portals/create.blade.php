@extends('layouts.app')

@section('header')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endsection

@section('content')

    <h4><a href="">Create new Portal</a></h4>
    <hr>

    <a href="/portal">Go back</a>
    </br>
    </br>
    
    <div class="card">
        <form action="/portal" method="post">
            {{csrf_field()}}
            <div class="form-group">
                <label for="name">Name:</label>
                <input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Create Portal</button>
            </div>
        </form>
    </div>


@endsection

@push('scripts')

    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

    <script>
        $(function () {
            $('#portals-table').DataTable(
                {
                    "order": [[1, "asc"]],
                    "pageLength": 10
                }
            );
        });
    </script>

@endpush