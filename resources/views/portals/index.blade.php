@extends('layouts.app')

@section('header')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endsection

@section('content')

    <h4><a href="">Portals</a></h4>
    <hr>

    <a href="/portal/create">Create new portal</a>
    </br>
    </br>

    <div class="container">
        <table class="table hover" id="portals-table">
            <thead>
            <tr>
                <th>Portal</th>
            </tr>
            </thead>
            @foreach($portals as $portal)
                <tr>
                    <td>
                        <a href="{{$portal->path()}}">  {{$portal->name}}  </a>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>


@endsection

@push('scripts')

    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>


    <script>
        $(function () {
            $('#portals-table').DataTable();
        });
    </script>

@endpush