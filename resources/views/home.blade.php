@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <li><a href="/portal">Portals</a></li>
                        <li><a href="/SKU">QB Inventory</a></li>
                        <li><a href="/multisets">Multisets</a></li>
                        <li><a href="/portal">Portals</a></li>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
