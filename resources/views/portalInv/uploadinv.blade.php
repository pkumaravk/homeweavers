@extends('layouts.app')

@section('header')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.css">

@endsection

@section('content')

    <h4>Upload Portal Inventory files</h4>

    <hr>

    <div class="col-md-6">
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Generate inv for Wayfair</button>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-success">Generate inv for Overstock</button>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-warning">Generate inv for Houzz</button>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-danger">Generate inv for Amazon Vendor Central</button>
        </div>
    </div>

    <div class="col-md-6">
        Upload Inventory file from portal here to update SKU codes:
        <form action="/upload" method="POST" class="dropzone" enctype="multipart/form-data">

            {{csrf_field()}}

        </form>
    </div>

    <hr>

    <div class="col-md-12">
        <form action="/upload" method="POST" enctype="multipart/form-data" class="form-control">
            <input type="file" name="file"/>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Import file</button>
            </div>


            {{csrf_field()}}
        </form>


    </div>

@endsection


@push('scripts')

    <script src="/js/dropzone.js"></script>


@endpush

