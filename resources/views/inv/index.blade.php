@extends('layouts.app')

@section('header')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endsection

@section('content')
    <div class="col-md-4">
        <h4>Quickbooks Inventory on hand</a></h4>
    </div>
    <div class="form-group col-md-8">
        <a href="/SKU/create" class="btn btn-danger">Upload file</a>
        <a href="/multisets/processMultisetInventory" class="btn btn-success">Process multisets</a>
    </div>

    <hr>

    <div class="container">
        <table class="table hover" id="sku-table">
            <thead>
            <tr>
                <th>Id</th>
                <th>SKU</th>
                <th>Description</th>
                <th>Qty</th>
            </tr>
            </thead>
            @foreach($skus as $sku)
                <tr>
                    <td>
                        {{$sku->id}}
                    </td>
                    <td>
                        <a href="">  {{$sku->code}}  </a>
                    </td>
                    <td>
                        {{$sku->description}}
                    </td>
                    <td>
                        {{$sku->qty}}
                    </td>
                </tr>
            @endforeach
        </table>
    </div>


@endsection

@push('scripts')

    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

    <script>
        $(function () {
            $('#sku-table').DataTable(
                {
                    "order": [[1, "asc"]],
                    "pageLength": 100
                }
            );
        });
    </script>

@endpush