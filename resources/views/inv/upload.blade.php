@extends('layouts.app')

@section('header')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.css">

@endsection

@section('content')

    <h4>Upload QuickBook Inventory in Excel format files</h4>

    <hr>

    <form action="/upload" method="POST" class="dropzone" enctype="multipart/form-data">

        {{csrf_field()}}
    </form>

    <div class="container">
        <form action="/importSKU" method="POST" enctype="multipart/form-data" class="form-control">
            <input type="file" name="file"/>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Import file</button>
            </div>
            {{csrf_field()}}
        </form>
    </div>

@endsection


@push('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.js"></script>


@endpush

