@extends('layout.app')

@section('header')
@endsection

@section('content')

    <h4>Upload quickbooks inventory files</h4>

    <hr>

<div class="container">
     <form action="/inv/store" method="POST" enctype="multipart/form-data" class="form-control">
            <input type="file" name="file"/>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Import file</button>
            </div>


            {{csrf_field()}}
        </form>
</div>


@endsection


@push('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.js"></script>


@endpush

