@extends('layout.app')

@section('header')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.css">

@endsection

@section('content')

    <h4>Upload order files</h4>

    <hr>

    <form action="/upload" method="POST" class="dropzone" enctype="multipart/form-data">
        {{ csrf_field() }}
    </form>



@endsection


@push('scripts')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.js"></script>


@endpush

