<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        User::create([
            'name' => 'SuperAdmin',
            'email' => 'pkumar@avkauto.com',
            'password' => bcrypt('password'),
        ]);

        User::create([
            'name' => 'Nitish',
            'email' => 'nitish23@gmail.com',
            'password' => bcrypt('Aanya26#'),
        ]);

        User::create([
            'name' => 'Swati',
            'email' => 'swati@homeweavers.net',
            'password' => bcrypt('Aanya26#'),
        ]);

        User::create([
            'name' => 'Shivani',
            'email' => 'shivani@homeweavers.net',
            'password' => bcrypt('Aanya26#'),
        ]);


    }
}
