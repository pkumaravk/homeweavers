<?php

use App\Http\Controllers\SkuController;
use App\Sku;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class QBInventorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = '/inv/seed/qb.xlsx';

        $data = Excel::load($file, function ($reader) {
        })->takeColumns(3)
            ->get();
        $recordsCreated = 0;
        $recordsUpdated = 0;

        if (!empty($data) && $data->count()) {
            foreach ($data as $key => $value) {
                if (!is_null($value->product)) {
                    $sku = sku::where('code', $value->product)
                        ->first();

                    if (!$sku) {
                        $sku = new Sku();
                        $sku->code = $value->product;
                        $sku->description = $value->description;
                        is_null($value->qty_on_hand) ? $sku->qty = 0 : $sku->qty = $value->qty_on_hand;
                        $sku->save();
                        $recordsCreated++;
                    } else {
                        $sku->description = $value->description;
                        is_null($value->qty_on_hand) ? $sku->qty = 0 : $sku->qty = $value->qty_on_hand;
                        $sku->update();
                        $recordsUpdated++;
                    }

//
                }
            }

        }

        echo 'Records updated: ' . $recordsUpdated . " and new Records created: " . $recordsCreated ."\n";
    }


}

