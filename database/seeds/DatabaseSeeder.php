<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        if (App::environment() === 'production') {
            exit('Dont create seeds in production !!');
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $tables = [
            'users',
        ];

        foreach ($tables as $table) {
            DB::table($table)->delete();
        }


         $this->call(UsersTableSeeder::class);
         $this->call(QBInventorySeeder::class);
         $this->call(MultisetSeeder::class);

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
