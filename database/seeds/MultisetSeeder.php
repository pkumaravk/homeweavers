<?php

use App\Sku;
use App\Multisets;
use Illuminate\Database\Seeder;

class MultisetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            $file = '/inv/seed/multisetdetailscsv.csv';

        $this->uploadCSVFile($file);
    }

    public function uploadCSVFile($file)
    {

        $records = Excel::load($file)->takeColumns(6)->get();

        foreach ($records as $row) {
            if (is_null($row->sku1) && is_null($row->sku2) && is_null($row->sku3) && is_null($row->sku4) && is_null($row->sku5)) {
                // echo 'record is null '. '</br>';
            } else {
                //    dump($row);

                //update or create
                // if (!is_null($row->sku1)) {
                $this->createMultiset($row->multiset, $row->sku1);
                $this->createMultiset($row->multiset, $row->sku2);
                $this->createMultiset($row->multiset, $row->sku3);
                $this->createMultiset($row->multiset, $row->sku4);
                $this->createMultiset($row->multiset, $row->sku5);
                // }
            }
        }
    }

    public function createMultiset($parentSKUCode, $childSKUCode)
    {
        if (isset($childSKUCode)) {
            $m = Multisets::where('childSKU_id', $childSKUCode)
                ->where('parentSKU_id', $parentSKUCode)
                ->first();
            $multiset = new Multisets();
            $multiset->parentSKU_id = Sku::where('code', $parentSKUCode)->value('id');
            $multiset->childSKU_id = Sku::where('code', $childSKUCode)->value('id');
            // dd($parentSKUCode, $childSKUCode, $multiset->parentSKU_id);
            if (isset($multiset->parentSKU_id) && isset($multiset->childSKU_id)) {
                $multiset->save();
                echo 'created mutliset ' . $parentSKUCode . '-' . $childSKUCode . '</br>';
            } else {
                echo 'could not create correctly the ' . $parentSKUCode . ' multiset </br>';

            }
        }

        return;
    }
}
