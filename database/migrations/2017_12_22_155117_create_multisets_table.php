<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMultisetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('multisets', function (Blueprint $table) {
            $table->increments('id');

            // $table->string('skuPiece');
            $table->integer('parentSKU_id')->unsigned();
            $table->integer('childSKU_id')->unsigned();

            // $table->integer('qty');
           $table->foreign('parentSKU_id')
               ->references('id')
               ->on('skus');
            $table->foreign('childSKU_id')
                ->references('id')
                ->on('skus');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('multisets');
    }
}
