<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortalSKUsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portal_SKUs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('portal_id')->unsigned();
            $table->integer('sku_id')->unsigned();
            $table->string('quickBooksCode');
            $table->string('portalCode');
            $table->foreign('portal_id')
                ->references('id')
                ->on('portals');
            $table->foreign('sku_id')
                ->references('id')
                ->on('SKUs');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portal_SKUs');
    }
}
