<?php

namespace App\Http\Controllers;

use App\Multisets;
use App\Sku;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class MultisetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $s = Sku::all();
        $s->load('children');

        $multisets = collect();
        foreach ($s as $item) {
            if ($item->children()->count() > 0)
                $multisets->push($item);
        }
        return view('multiset.index', compact('multisets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('multiset.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $multiset = '';

        return view('multisets.show', compact('multiset'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function import()
    {
        return view('multiset.upload');
    }


    public function uploadExcelFile(Request $request)
    {
        // dd('reached here');

        $records = Excel::load($request->file('file'))->takeColumns(6)->get();

        // return $records;

        foreach ($records as $row) {
            if (is_null($row->sku1) && is_null($row->sku2) && is_null($row->sku3) && is_null($row->sku4) && is_null($row->sku5)) {
                // echo 'record is null '. '</br>';
            } else {
                //    dump($row);

                //update or create
                // if (!is_null($row->sku1)) {
                $this->createMultiset($row->multiset, $row->sku1);
                $this->createMultiset($row->multiset, $row->sku2);
                $this->createMultiset($row->multiset, $row->sku3);
                $this->createMultiset($row->multiset, $row->sku4);
                $this->createMultiset($row->multiset, $row->sku5);

                // }
            }
        }


        // return $records; 

// isNull()
    }

    public function createMultiset($parentSKUCode, $childSKUCode)
    {
        if (isset($childSKUCode)) {


            $m = Multisets::where('childSKU_id', $childSKUCode)
                ->where('parentSKU_id', $parentSKUCode)
                ->first();


            $multiset = new Multisets();
            $multiset->parentSKU_id = SKU::where('code', $parentSKUCode)->value('id');
            $multiset->childSKU_id = SKU::where('code', $childSKUCode)->value('id');
            // dd($parentSKUCode, $childSKUCode, $multiset->parentSKU_id);
            if (isset($multiset->parentSKU_id) && isset($multiset->childSKU_id)) {
                $multiset->save();
                echo 'created mutliset ' . $parentSKUCode . '-' . $childSKUCode . '</br>';
            } else {
                echo 'could not create correctly the ' . $parentSKUCode . ' multiset </br>';

            }
        }

        return;
    }

    public function processQty()
    {
        $s = Sku::all();
        $s->load('children');

        $multisets = collect();
        foreach ($s as $item) {
            if ($item->children()->count() > 0)
                $multisets->push($item);
        }

//        return $multisets;
        foreach ($multisets as $row) {
            $calc = 100000000;

            //            dump($row);
            foreach ($row->children()->get() as $items) {
                $child = Sku::where('id', $items->childSKU_id)->first();
                $calc = min($calc, $child->qty);
            }

            $row->qty = $calc;
            $row->update();
        }

        dd('end');
        return back();
    }
}
