<?php

namespace App\Http\Controllers;

use App\Portal;
use App\PortalInventory;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Facades\Excel;

class InventoryWriterController extends Controller
{
    public function index()
    {

//        return view('inventory.index');
        return view('inv.uploadinv');
    }


    public function upload(Request $request)
    {

//        return $request->file('file')->getClientOriginalName();

        $records = Excel::load($request->file('file'))->get();
        

        if ($records->first()->supplier_id == '19440') {
            $this->parseFileWayfair($records);
        }

        if (stripos(request()->file('file')->getClientOriginalName(), 'overstock') !== false) {
            $this->parseFileOverstock($request);
        }



        


        return $records;

        // return 'main function';
        return $r->first()->supplier_id;


        $file = request()->file('file')->storeAs('inventoryfiles', request()->file('file')->getClientOriginalName());

        if (stripos(request()->file('file')->getClientOriginalName(), 'overstock') !== false) {
            return response('overstock inventory file received', 200);
        }

        return response(request()->file('file')->getClientOriginalName(), 200);

        return view('.upload', compact(''));
    }

    public function parseFileOverstock(Request $request)
    {
        $r = Excel::load($request->file('file'))->get();

        echo 'reached overstock';
        $portal = Portal::where('id', 'Overstock')->first();

        $portalInventory = PortalInventory::where('SKUCode', $r->first()->supplier_sku)->get();
//        $portalInventory = PortalInventory::where('SKUCode', 'ABC7654321')->get();

        foreach ($r as $row) {
            $portalInventory = PortalInventory::where('SKUCode', $row->supplier_sku)->get();

            if ($portalInventory) {
                $p = new PortalInventory();
                $p->portalId = 1;
                $p->qty = $row->quantity;
                $p->SKUCode = $row->supplier_sku;
                $p->save();
            }
        }

//        return $r->first();


        dd('processing overstock file');
    }


    public function parseFileWayfair($records)
    {
        // dd($records->first());

        $portalId = 2;  //set this later

        foreach ($records as $row) {
            $this->updatePortalInventoryRecord($portalId, $row->supplier_part, $row->quantity_on_hand);
        }

        dd('processing wayfair file');
    }

    private function updatePortalInventoryRecord($portalId, $SKUCode, $qty) {

        $portalInventory = PortalInventory::where('SKUCode', $SKUCode)
        ->where('portalId', $portalId)
        ->first();

        // if(!$portalInventory) {
        //     dd('new record must be created');
        // } else {
        //     dd('old record must be updated');
        // }


// dd($portalInventory);

        if (!$portalInventory) {
            $p = new PortalInventory();
            $p->portalId = $portalId;
            $p->qty = $qty;
            $p->SKUCode = $SKUCode;
            $p->save();
        } else {
            $portalInventory->qty = $qty;
            $portalInventory->update();
        }
    }

    public function overstock()
    {

//        return ('reached csv');


        $r = Excel::load('Overstock_Inv.csv')->get();

//        var_dump(get_class_methods($r));


//        return $r->first();

        Excel::create('Overstock_InvG', function ($excel) use ($r) {
            $excel->sheet('Overstock_InvG', function ($sheet) use ($r) {
                $sheet->fromArray($r, null, 'A1', true);
                $sheet->cell('A1', function ($cell) {

                    // manipulate the cell
                    $cell->setValue('Supplier Sku');
                });

                $sheet->setCellValue('A2', 'Test');
            });
        })->export('csv');

        return ('processed file with errors');


//        return $r;

//        foreach($r as $row){
//
//            dump($row->supplier_sku);
//        }
//
//

//        dd($r);
//            ->each(function (Collection $csvLine) {
//
//            echo $csvLine->get('supplier_sku') ."<br>";
//
////            Character::create([
////                'name' => "{$csvLine->get('first_name')} {$csvLine->get('last_name')}",
////                'job' => $csvLine->get('job'),
////            ]);
//
//        });
    }
}
