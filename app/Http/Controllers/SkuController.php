<?php

namespace App\Http\Controllers;

use App\Sku;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class SkuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $skus = Sku::get();
        return view('inv.index', compact('skus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('inv.upload');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

//    public function ImportInv(Request $request)
    public static function ImportInv($file)
    {

        $records = Excel::load($request->file('file'))->takeColumns(6)->get();

        $path = request()->file('file')->getRealPath();

        $data = Excel::load($request->file('file'), function ($reader) {
        })->takeColumns(3)
            // ->skipRows(4)
            ->get();

        // return $data;

        $recordsCreated = 0;
        $recordsUpdated = 0;


        if (!empty($data) && $data->count()) {
            foreach ($data as $key => $value) {
                if (!is_null($value->product)) {

                    $sku = sku::where('code', $value->product)
                        ->first();

                    if (!$sku) {
                        $sku = new SKU();
                        $sku->code = $value->product;
                        $sku->description = $value->description;
                        is_null($value->qty_on_hand) ? $sku->qty = 0 : $sku->qty = $value->qty_on_hand;
                        $sku->save();
                        $recordsCreated++;
                    } else {

                        $sku->description = $value->description;
                        is_null($value->qty_on_hand) ? $sku->qty = 0 : $sku->qty = $value->qty_on_hand;
                        $SKU->update();
                        $recordsUpdated++;
                    }

//
                }
            }

        }

        return ('Records updated:' . $recordsUpdated . " and new Records created: " . $recordsCreated);
    }


}
