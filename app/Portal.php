<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Portal extends Model
{
    protected $fillable=['name'];

    public function path()
    {
        return '/portal/'.$this->id;
    }



}
