<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sku extends Model
{
    protected $table='skus';

  public function children()
    {

        return $this->hasMany(Multisets::class, 'parentSKU_id');
    }
}
