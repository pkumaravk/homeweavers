<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Multisets extends Model
{
    protected $fillable = ['parentSKU_id', 'childSKU_id'];

    public function parentSku()
    {

        return $this->belongsTo(Sku::class, 'parentSKU_id');
    }

    public function childSku()
    {

        return $this->hasMany(Sku::class, 'id');
    }


}
