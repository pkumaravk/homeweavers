<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', function () {
    return view('welcome');
});

Route::resource('SKU','SKUController');
Route::post('importSKU', 'SKUController@ImportInv');


Route::resource('portal', 'PortalsController');



// Route::resource('order','OrderController');

Route::get('csv', 'InventoryWriterController@overstock');
Route::get('uploadQBInventory','InventoryWriterController@index');
Route::post('upload','InventoryWriterController@upload');

Route::get('/multisets/processMultisetInventory', 'MultisetController@processQty');
Route::get('/multisets/importMulti', 'MultisetController@import');
Route::resource('multisets', 'MultisetController');
Route::post('uploadMulti', 'MultisetController@uploadExcelFile');


Route::get('test', function(){

    dd(base_path());


});